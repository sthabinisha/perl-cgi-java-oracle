# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

function cd {
    builtin cd "$@" && ls -F
}

# export SYSTEMD_PAGER=

# User specific aliases and functions

PS1="01:\w> "



# PS1="[\[\033[32m\]\w]\[\033[0m\]\n\[\033[1;36m\]\u\[\033[1;33m\]-> \[\033[0m\]"

alias ll='ls -l'
alias rm='rm -i'
PS1="\w> "

# Greeting
echo -e "\n\e[1;31mWelcome master. You are logged on \e[0m\n"
uname -a
echo ""
date
echo ""

#PS1="[\u@\h:\W, \@, \d]> " #custom prompt options
alias dir='ls -Flg | more'
alias h='history'
alias cp='cp -i'    # don't overwrite existing files without ok
alias mv='mv -i'    # don't overwrite existing files without ok
alias rm='rm -i'    # don't overwrite existing files without ok
alias ls='ls --color=auto -F'
prompt='%B<%m:%3.>%b$ '
function lc () {
    cd $1;
    ls
}

PS1="01:\w> "

# Uncomment the following line if you don't like systemctl's auto-paging feature:

CLASSPATH=.:/usr/lib/oracle/12.1/client64
CLASSPATH=$CLASSPATH:/usr/lib/oracle/12.1/client64/lib/ojdbc7.jar
CLASSPATH=$CLASSPATH:/usr/lib/oracle/12.1/client64/lib/ottclasses.zip
export  CLASSPATH

export ORACLE_HOME=/usr/lib/oracle/12.2/client64
export LD_LIBRARY_PATH=${ORACLE_HOME}/lib
export PATH=${ORACLE_HOME}/bin:$PATH
export  CLASSPATH

# Java
# export DISPLAY=:0.0
