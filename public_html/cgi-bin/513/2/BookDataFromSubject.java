

// Import the following packages to use JDBC.
import  java.sql.*;
import  java.io.*;
import  oracle.jdbc.*;
import  oracle.jdbc.pool.OracleDataSource;

class  BookDataFromSubject {
  public static void  main( String args[ ] ) throws SQLException {
    String user     = "C##b.shrestha";
    String password = "shrestha9065";
    String database = "65.52.222.73:1521/cdb1";

    // Open an OracleDataSource and get a connection.
    OracleDataSource ods = new OracleDataSource( );
    ods.setURL     ( "jdbc:oracle:thin:@" + database );
    ods.setUser    ( user );
    ods.setPassword( password );
    Connection conn = ods.getConnection( );
    String query, subQuery, username, subject;
    int num= 1;


    try {
      // Create, compose, and execute a statement.
      Statement stmt = conn.createStatement( );
      ResultSet rset;
      subject = args[1].trim();
       if ( args[0].equals( "subjectdata" ) ) {
       	query= "select distinct b.isbn, b.title, price from bookstore_dtls b, Table(b.subject_dtls) s ,subject_table d where d.subject_name ='"+subject+"' and d.subject_id = s.column_value";
       	rset = stmt.executeQuery(query);
       	int i = 1;
        System.out.println("<br/><br/><b>");

       	System.out.println("Book Subject "+ num+": " +  "<a href='http://undcemcs02.und.edu/~b.shrestha/cgi-bin/513/2/BookHiperlink.cgi?title=" + subject + "&subject=checked'>" + subject + "<a>" );
	System.out.println("<br/><br/><b>");  
       	while(rset.next()){
                System.out.println("<br/><br/><b>");
                System.out.println("Book ISBN: "+ rset.getString(1) );
                System.out.println("<br/><br/><b>");
                System.out.println("Book Title: "+  "<a href='http://undcemcs02.und.edu/~b.shrestha/cgi-bin/513/2/BookHiperlink.cgi?title=" + rset.getString(2) +"'>"+ rset.getString(2) + "<a>" );
                System.out.println("<br/><br/><b>");
                System.out.println("Book Price: "+ rset.getString(3) );
                System.out.println("<br/><br/><b>");
 
       	}
       	rset.close();



       }
       stmt.close();
    }
    
    catch ( SQLException ex ) {
      System.out.println( ex );
    }
    // Close the Connection.
    conn.close( );
  }
}




