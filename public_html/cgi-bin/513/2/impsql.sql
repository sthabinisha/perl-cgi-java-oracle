CREATE TYPE OBJ_TYPE_SUBJ IS OBJECT
(
   SUBJECT_ID Number,
   Subject_name Varchar(64)
);
CREATE TYPE NT_Subject_table IS TABLE OF OBJ_TYPE_SUBJ;

CREATE TABLE BookStore_DTLS
(
   ISBN           char(10),
   title      VARCHAR2(60),
   price     NUMBER,
   subject_dtls      NT_Subject_table
) NESTED TABLE subject_dtls STORE AS SYS_GEN

INSERT INTO BookStore_DTLS(ISBN,
                               title,
                               price,
                               subject_dtls)
      VALUES(12345,
                  'Harry Potter',
                  10,
                  NT_Subject_table(
                                              OBJ_TYPE_SUBJ (id_seq.nextval, 'Physics'),
                                              OBJ_TYPE_SUBJ (id_seq.nextval, 'Maths')

                                             )
                );
                
                select * from bookstore_dtls
                
                SELECT *
  FROM TABLE(
                        SELECT subject_dtls
                           FROM BookStore_DTLS
                            WHERE title = 'Harry Potter'
                      );
                      
