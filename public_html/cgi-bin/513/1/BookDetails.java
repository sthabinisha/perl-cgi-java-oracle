
/*******************************************************************


  This program shows how to list the book titles in the
                stmt.execute("Inset into book_sub values(id_seq.NextVal, '"+rset.getString(1) +"'");
    inventory table.

  To use this program, you need to create a table  by using the following commands:

  SQL> create table bookstore ( isbn char(10) primary key, title varchar(64) not NULL, price number(8,2) not NULL check( price    >= 0.0);
  SQL> create table book_sub (isbn char(10), subjectid INTEGER);
  SQL> create table subject ( subjectid INTEGER primary key, subject varchar(64));

  Table created.

 SQL> insert into subject values ('1', 'Computer Science');
 SQL> insert into subject values ('2', 'Programming Languages');
 SQL> insert into subject values ('3', 'Operating Systems');
 SQL> insert into subject values ('4', 'Graphics');


 SQL> insert into BOOKSTORE values ('20060101','ordinary grace','89');
 SQL> insert into BOOKSTORE values ('20060102','ApGwilym','100');
 SQL> insert into BOOKSTORE values ('20060103','Zola','65.5');

 SQL> insert into book_sub values ('20060101', '1');
 SQL> insert into book_sub values ('20060102', '2');

 SQL> insert into book_sub values ('20060103', '3');

 SQL> CREATE SEQUENCE id_seq MINVALUE 1
START WITH 1 INCREMENT BY 1 NOCACHE;

Sequence created.
commit;
*******************************************************************/

// Import the following packages to use JDBC.
import  java.sql.*;
import  java.sql.DatabaseMetaData;
import  java.io.*;
import  java.math.BigDecimal;

import  oracle.sql.*;
import  oracle.jdbc.*;
import  oracle.jdbc.pool.OracleDataSource;

class  booksdetail {
    public static void  main( String args[] ) throws SQLException {
        String user     = "C##b.shrestha";
        String password = "shrestha9065";
        String database = "65.52.222.73:1521/cdb1";

        // Open an OracleDataSource and get a connection.
        OracleDataSource ods = new OracleDataSource( );

        ods.setURL     ( "jdbc:oracle:thin:@" + database );
        ods.setUser    ( user );
        ods.setPassword( password );
        Connection conn = ods.getConnection( );
	try {
        Statement stmt = conn.createStatement( );
	Statement stmt1 = conn.createStatement( );
	Statement stmt2 = conn.createStatement( );

        String query, subjectInsert, book_subInsert, cmd, subjectID;
        int subject= 0;
        ResultSet rset;
        ResultSet rset1;
        ResultSet rset2;

        String queryCount= "";

        String subjectname= "";
	subjectID= "";



      if ( args[0].equals( "selectbooktitle" ) ) {
 	     System.out.println("I am here");
 	   
            query = "select  b.title, b.isbn, b.price, s.subjectname from bookstore b inner join book_sub bs on bs.isbn=b.isbn inner join subject s on s.subjectid=bs.subjectid where b.title='" + args[1].trim( ) + "'";
            System.out.println(query);
            rset = stmt.executeQuery( query );
            while ( rset.next( ) ) {
                System.out.println("<br/><br/><b>");
                System.out.println("Book ISBN: "+ rset.getString(2) );
                System.out.println("<br/><br/><b>");
                System.out.println("Book Title: "+ rset.getString(1) );
                System.out.println("<br/><br/><b>");
                System.out.println("Book Price: "+ rset.getString(3) );
                System.out.println("<br/><br/><b>");
                System.out.println("Book Subject: "+  "<a href='http://undcemcs02.und.edu/~b.shrestha/cgi-bin/513/1/BookHiperlink.cgi?name=" + rset.getString(4) + "&subject=checked'>" + rset.getString(4) + "<a>" );
                System.out.println("<br/><br/><b>");

            
             }
	   rset.close();
	   stmt.close();
           }
         else if ( args[0].equals( "selectbooksubject" ) ) {
             System.out.println("I am here");
            query = "select  b.title from bookstore b inner join book_sub bs on bs.isbn=b.isbn inner join subject s on s.subjectid=bs.subjectid where s.subjectname='" + args[1].trim( ) + "'";
            System.out.println(query);
            rset = stmt.executeQuery( query );
            int i = 1;
            while ( rset.next( ) ) {
                System.out.println("<br/><br/><b>");
                System.out.println("Book Title "+ i +" :"+  "<a href='http://undcemcs02.und.edu/~b.shrestha/cgi-bin/513/1/BookHiperlink.cgi?name=" + rset.getString(1) + "'>" + rset.getString(1) + "<a>" );
                System.out.println("<br/><br/><b>");
                i++;

             }
          rset.close( );
	  stmt.close();
          conn.close();

       }

    stmt.close( );
    conn.close( );
	}
	catch( Exception ex ) {
	    ex.printStackTrace( );
	}
  }
}


